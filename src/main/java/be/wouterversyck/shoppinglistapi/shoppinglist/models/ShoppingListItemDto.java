package be.wouterversyck.shoppinglistapi.shoppinglist.models;

public interface ShoppingListItemDto {
    long getId();
    String getName();
}
